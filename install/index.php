<?
IncludeModuleLangFile(__FILE__);

class chernov_superfeedback extends CModule
{
    const MODULE_ID = 'chernov.superfeedback';

    var $MODULE_ID = 'chernov.superfeedback';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {
        $arModuleVersion = array();

        include(dirname(__FILE__) . "/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = GetMessage('SFB_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('SFB_MODULE_DESCRIPTION');
    }

    function InstallDB()
    {

    }

    function UnInstallDB()
    {

    }

    function InstallFiles()
    {
        CopyDirFiles(dirname(__FILE__)."/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
        CopyDirFiles(dirname(__FILE__)."/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);
	}

    function UnInstallFiles()
    {
		
    }

    function RegisterEvents()
    {

    }

    function UnRegisterEvents()
    {

    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        $this->InstallFiles();
        $this->InstallDB();
        $this->RegisterEvents();
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnRegisterEvents();
        UnRegisterModule($this->MODULE_ID);
    }

}
